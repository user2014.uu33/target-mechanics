UFUNCTION()
void RotationAngleCalculation(float DeltaTime);

UFUNCTION()
void TargetCamera(float DeltaTime);

TArray<AActor*> TargetCameraActors(float Radius);

UFUNCTION()
bool AddToIgnoreTargets(AActor* OverlapActor);

bool IsTarget;

UPROPERTY()
AActor* AvailableTargets;

UPROPERTY()
TArray<AActor*> InteractionTargetActors;

UPROPERTY()
TArray<AActor*> TempEqually;

UFUNCTION()
AActor* FindTargetFromOut(TArray<AActor*> First, TArray<AActor*> Second);

int32 TotalValue;