void ASC_PlayerController::RotationAngleCalculation(float DeltaTime)
{
    if (PossessedCharacter)
    {
        FVector CapsuleLocation = PossessedCharacter->GetCapsuleComponent()->GetComponentLocation();
        FVector SpringArmLocation = PossessedCharacter->GetCameraBoom()->GetComponentLocation();

        float DistBV = FVector::Distance(CapsuleLocation, SpringArmLocation);

        if(DistBV > PossessedCharacter->IdleCameraArea)
        {
            FVector GetDir = UKismetMathLibrary::GetDirectionUnitVector(CapsuleLocation, SpringArmLocation);
            GetDir *= PossessedCharacter->IdleCameraArea;
            GetDir += PossessedCharacter->GetCapsuleComponent()->GetComponentLocation();
            
            PossessedCharacter->GetCameraBoom()->SetWorldLocation(FMath::VInterpTo(SpringArmLocation, GetDir,
                DeltaTime, PossessedCharacter->ChangingPursuitInterpolation));

            if(!IsTarget)
            {
                float X,Y,Z;
                UKismetMathLibrary::BreakRotator(PossessedCharacter->GetControlRotation(), X,Y, Z);
                FRotator MakeRot = UKismetMathLibrary::MakeRotator(0,0,Z);
	
                FVector ForwardVector = UKismetMathLibrary::GetForwardVector(MakeRot);
                FVector RightVector = UKismetMathLibrary::GetRightVector(MakeRot);
		
                FVector AxisVectorForward = ForwardVector * PossessedCharacter->MoveForwardAxis;
                FVector AxisVectorRight =  RightVector * PossessedCharacter->MoveRightAxis;
		
                FVector SumVector =  AxisVectorForward + AxisVectorRight;
		
                float DotProduct = 1. - FMath::Abs(FVector::DotProduct(ForwardVector, SumVector)); 
                float FirstRange = FMath::Clamp(FMath::Abs(PossessedCharacter->MoveForwardAxis) +
                    FMath::Abs(PossessedCharacter->MoveRightAxis), 0, 1);
                float SecondRange = FMath::Clamp(DeltaTime * FirstRange * DotProduct * PossessedCharacter->TurningSpeed, 0, 1);
		        
                PossessedCharacter->RewritingValueForCameraRotation(SecondRange * (UKismetMathLibrary::NormalizedDeltaRotator(
                    UKismetMathLibrary::MakeRotFromX(SumVector), PossessedCharacter->GetControlRotation()).Yaw));
            }
        }
    }
}

void ASC_PlayerController::TargetCamera(float DeltaTime)
{
    if (PossessedCharacter)
    {
        TempEqually = TargetCameraActors(PossessedCharacter->MaxTargetLockDistance);
        
        if(TempEqually.Num() != TotalValue) TotalValue = TempEqually.Num() - TotalValue;
        
        if(TempEqually.IsEmpty() || !PossessedCharacter->StartTargeting)
        {
            InteractionTargetActors.Empty();

            if(IsTarget) PossessedCharacter->Server_SetMovementMode(ESC_MovementMode::Directional);

            IsTarget = false;
            PossessedCharacter->StartTargeting = false;
            TotalValue = 0;
            return;
        }

        if(InteractionTargetActors.Num() == 1 || PossessedCharacter->CurrentTargetIt > InteractionTargetActors.Num() - 1)
        {
            PossessedCharacter->CurrentTargetIt = 0;
        }
        
        if(TempEqually.Num() != InteractionTargetActors.Num())
        {
            if(TempEqually.Num() > InteractionTargetActors.Num())
            {
                if(TotalValue == 1)
                {
                    AActor* SetNewTarget = FindTargetFromOut(TempEqually, InteractionTargetActors);
                    if(SetNewTarget != nullptr)
                        InteractionTargetActors.Add(SetNewTarget);
                    else
                        InteractionTargetActors.Add(TempEqually.Last());
                }
                else
                {
                    for(int i = 0; i < TempEqually.Num(); ++i)
                    {
                        InteractionTargetActors.Add(TempEqually[i]); 
                    }
                }
                TotalValue = InteractionTargetActors.Num();
            }
            else if(TempEqually.Num() < InteractionTargetActors.Num())
            {
                AActor* CurrentTarget = InteractionTargetActors[PossessedCharacter->CurrentTargetIt];
                for(int i = 0; i < TempEqually.Num(); ++i)
                {
                    if(CurrentTarget == TempEqually[i])
                    {
                        //AActor* SetNewTarget = FindTargetFromOut(InteractionTargetActors, TempEqually);
                        //InteractionTargetActors.Remove(SetNewTarget);
                        break;
                    }
                    
                    else if((TempEqually.Num() - 1) == i) PossessedCharacter->StartTargeting = false;
                }
            }
        }       
        if(!IsTarget) PossessedCharacter->Server_SetMovementMode(ESC_MovementMode::Strafe);

        IsTarget = true;

        TArray<AActor*> CurrentActor;

        for(auto c : InteractionTargetActors)
            CurrentActor.Add(c);
        FVector TargetVect = InteractionTargetActors[PossessedCharacter->CurrentTargetIt]->GetActorLocation() -
            PossessedCharacter->GetCameraBoom()->GetComponentLocation();
			
        FRotator TargetRot = TargetVect.GetSafeNormal().Rotation();
        FRotator CurrentRot = GetControlRotation();
        FRotator NewRot = FMath::RInterpTo(CurrentRot, TargetRot, DeltaTime, PossessedCharacter->InterpSpeedOnTarget);
        if (GetLocalRole() == ROLE_AutonomousProxy)
        {
            SetControlRotation(NewRot);
        }
    }
}

AActor* ASC_PlayerController::FindTargetFromOut(TArray<AActor*> First, TArray<AActor*> Second)
{
    for(int i = 0; i < First.Num(); ++i)
        for(int j = 0; j < Second.Num(); ++j)
        {
            if(First[i] == Second[j])
                break;
            else if(First[i] != Second[j] && j == Second.Num() - 1)
            {
                return First[i];
            }
        }
    return nullptr;
}

TArray<AActor*> ASC_PlayerController::TargetCameraActors(float Radius)
{
    TArray<FOverlapResult> OverlapResults;
	
    GetWorld()->OverlapMultiByChannel(OverlapResults, PossessedCharacter->GetActorLocation(), PossessedCharacter->GetActorQuat(),
        ECC_GameTraceChannel2, FCollisionShape::MakeSphere(Radius));

    DrawDebugSphere(GetWorld(), PossessedCharacter->GetCapsuleComponent()->GetComponentLocation(), Radius, 32, FColor::Cyan);

    //TArray<AActor*> InteractionActors;
    TArray<AActor*> InteractionActors;
    for (FOverlapResult Overlap : OverlapResults)
    {
        AActor* OverlapActor = Overlap.GetActor();
        
        if (OverlapActor != nullptr)
            if(AddToIgnoreTargets(OverlapActor))
                InteractionActors.Add(OverlapActor);
    }

    return InteractionActors;
}

bool ASC_PlayerController::AddToIgnoreTargets(AActor* OverlapActor)
{
    FVector A = PossessedCharacter->GetFollowCamera()->GetForwardVector();
    FRotator Temp = UKismetMathLibrary::FindLookAtRotation(PossessedCharacter->GetFollowCamera()->GetComponentLocation(),
        OverlapActor->GetActorLocation());
    FVector B = UKismetMathLibrary::GetForwardVector(Temp);
    
    float angle = FMath::Acos(FVector::DotProduct(A, B));

    if(angle >= (PossessedCharacter->AngleTargetsToTriggered / 100.f))
        return false;
    else
        return true;
}

